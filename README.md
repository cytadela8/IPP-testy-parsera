# Testy jednostkowe do parsera z drugiej części zadania Telefony

## Odpalanie
Testy mają taką formę, że da się je testować skryptem z małego projektu. Pamiętajcie o testowaniu z valgrindem, bo mamy zwalniać całą pamięć w przypadku błędu :p

## Dorzucanie się do repo
Trzeba zrobić forka (przycisk na górze strony), spushować swoje zmiany i zrobić merge requesta (https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

Niech testy mają sensowną nazwę i postać

sensowna_nazwa.in

sensowna_nazwa.out

sensowna_nazwa.err